package com.phonecompany.billing;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class TelephoneBillCalculatorImpl implements TelephoneBillCalculator {


	@Override
	public BigDecimal calculate(String phoneLog) {
		String[] log = phoneLog.split(",|\n");
		LocalTime start=LocalTime.of(8, 0, 0, 0);
		LocalTime end=LocalTime.of(16, 0, 0, 0);
		Map<String, Pair<BigDecimal,Integer>> charge = new HashMap<>();
		for(int i=0;i<log.length;i=i+3){
			Pair<BigDecimal,Integer> cost = charge.getOrDefault(log[i],new Pair<BigDecimal,Integer>(BigDecimal.ZERO,0));
			DateTimeFormatter dTF = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
			LocalDateTime t1 = LocalDateTime.parse(log[i+1],dTF);
			LocalDateTime t2 = LocalDateTime.parse(log[i+2],dTF);
			int minutes = 0;
			while(!Duration.between(t1.toInstant(ZoneOffset.UTC),t2.toInstant(ZoneOffset.UTC)).isNegative()&&minutes<5){
				if(t1.toLocalTime().isAfter(start)&&t1.toLocalTime().isBefore(end)){
					cost.setFirst(cost.getFirst().add(BigDecimal.ONE));
				}else{
					cost.setFirst(cost.getFirst().add(BigDecimal.valueOf(0.5)));
				}
				t1=t1.plusMinutes(1);
				minutes++;
			}
			if(!Duration.between(t1.toInstant(ZoneOffset.UTC),t2.toInstant(ZoneOffset.UTC)).isNegative()){
				int remainder = (int) Duration.between(t1.toInstant(ZoneOffset.UTC),t2.toInstant(ZoneOffset.UTC)).toMinutes();
				cost.setFirst(cost.getFirst().add((BigDecimal.valueOf(remainder*0.2))));
			}
			cost.setSecond(cost.getSecond()+1);
			charge.put(log[i],cost);
		}

		String key = charge.entrySet().stream().max(
				Comparator.comparing(i -> i.getValue().getSecond()))
				.orElseThrow(NoSuchElementException::new).getKey();
		BigDecimal all = BigDecimal.ZERO;
		for(var tmp : charge.entrySet()){
			if(!tmp.getKey().equals(key)){
				all=all.add(tmp.getValue().getFirst());
			}
		}
		return all;
	}
}
